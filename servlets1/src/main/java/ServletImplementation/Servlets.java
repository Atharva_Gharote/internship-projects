package ServletImplementation;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Servlets extends HttpServlet
 {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        PrintWriter out = response.getWriter();
        String userName = request.getParameter("name");
        out.println("<html><body>");
        out.println("<h1>Hello "+userName+"</h1>");
        out.println("</body></html>");
    }
    @Override
    public void doPut(HttpServletRequest request,HttpServletResponse response) throws IOException
    {
        PrintWriter out = response.getWriter();
        out.println("Put Method is Invoked");
    }

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException
    {

        PrintWriter out = response.getWriter();
        String message = request.getParameter("message");
        out.println("<html><body>");
        out.println("<h1>"+message+"</h1>");
        out.println("</body></html>");
    }
    @Override
    public void doDelete(HttpServletRequest request,HttpServletResponse response) throws IOException
    {
        PrintWriter out = response.getWriter();
        out.println("Delete Method is Invoked");
    }

 }
