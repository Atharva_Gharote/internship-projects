
CREATE TABLE users

  (id               BIGSERIAL PRIMARY KEY NOT NULL ,
  first_name        TEXT ,
  last_name         TEXT ,
  email             TEXT,
  city              TEXT,
  course_name       TEXT,
  user_password     TEXT,
  course_duration   INTEGER,
  creation_stamp    TIMESTAMP NOT NULL );