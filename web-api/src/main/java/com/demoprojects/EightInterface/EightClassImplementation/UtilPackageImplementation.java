package com.demoprojects.EightInterface.EightClassImplementation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Base64;
import java.util.*;
import java.lang.*;

public class UtilPackageImplementation
{
    private static final Logger logger = LogManager.getLogger(UtilPackageImplementation.class);
    private static Scanner scanner=new Scanner(System.in);

    public void parallelSorting()
    {
        int array[]={1,2,3,4,6666,555};
        Arrays.parallelSort(array);
        for (int x:array)
        {
          logger.info("{}",x);
        }
    }
    public void encodeDecode()
    {
        logger.trace("Enter the String to encode");
        String string=scanner.nextLine();
        Base64.Encoder encoder=Base64.getEncoder();
        String encodedString = encoder.encodeToString(string.getBytes());
        logger.info("String {} Encoded into {}",string,encodedString);
        Base64.Decoder decoder=Base64.getDecoder();
        String decodedString =new String(decoder.decode(encodedString));
        logger.info("Decoded String {}",decodedString);
        logger.trace("Enter the Url to encode");
        String url=scanner.nextLine();
        Base64.Encoder encoderUrl=Base64.getUrlEncoder();
        String encodedUrlString = encoderUrl.encodeToString(url.getBytes());
        logger.info("String {} Encoded into {}",url,encodedUrlString);
        Base64.Decoder decoderUrl=Base64.getUrlDecoder();
        String decodedUrlString =new String(decoderUrl.decode(encodedUrlString));
        logger.info("Decoded String {}",decodedUrlString);
    }

    public static void main(String[]args)
    {
        UtilPackageImplementation utilPackage =new UtilPackageImplementation();
        utilPackage.parallelSorting();
        utilPackage.encodeDecode();




    }
}
