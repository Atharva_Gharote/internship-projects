package com.demoprojects.EightInterface.EightClassImplementation;

import com.demoprojects.EightInterface.TypeInferenceExample;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

public class TypeInference
 {
     private static final Logger logger = LogManager.getLogger(TypeInference.class);
     public static void main(String args[])
     {
         Scanner scanner=new Scanner(System.in);
         TypeInferenceExample<Integer,Integer,Integer> typeInferenceExample=(number1, number2)->number1*number2;
         logger.trace("Enter two Numbers for Multiplication");
         int number1=scanner.nextInt();
         int number2=scanner.nextInt();
         int number3=typeInferenceExample.multiply(number1,number2);
         logger.info("Result  {}",number3);
     }
 }

