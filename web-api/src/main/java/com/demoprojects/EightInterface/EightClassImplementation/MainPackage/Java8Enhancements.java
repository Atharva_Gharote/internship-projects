package  com.demoprojects.EightInterface.EightClassImplementation.MainPackage;
import com.demoprojects.EightInterface.NumberTest;
import com.demoprojects.EightInterface.Addable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.demoprojects.EightInterface.EightClassImplementation.DefaultMethods;
import java.util.Scanner;

public class Java8Enhancements
 {
     private static final Logger logger = LogManager.getLogger(Java8Enhancements.class);

    public static void addNumber()
    {
       Scanner scanner = new Scanner(System.in);
       logger.info("Enter 2 Numbers for addition");
       int number1 = scanner.nextInt();
       int number2 = scanner.nextInt();
       int result = number1 + number2;
       logger.info("Addition is {}", result);

    }
    public static void main(String args[])
   {
        Scanner scanner =new Scanner(System.in);
        logger.info("Enter number to determine even or odd ");
        NumberTest test =() -> {
        int numberTest=scanner.nextInt();
        return ((numberTest%2) == 0);};
        boolean result=test.computeTest();
        if(result)
        {
           logger.info("Even number");
        }
        else
        {
            logger.info("odd number");
        }
        Addable addable=Java8Enhancements::addNumber;
        addable.addNumber();
        DefaultMethods defaultMethodObject =new DefaultMethods();
        defaultMethodObject.printMessage();

   }

 }
