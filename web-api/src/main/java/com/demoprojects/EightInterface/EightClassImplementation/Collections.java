package com.demoprojects.EightInterface.EightClassImplementation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;
import java.util.stream.Collectors;


class Collections
 {
     private static final Logger logger = LogManager.getLogger(Collections.class);
     public static void main(String[] args)
     {
         List<Integer> list=new ArrayList<>();
         list.add(2355);
         list.add(346);
         list.add(45666);
         list.add(555_555);
         list.forEach(System.out::println);
         List<Integer> integerList=list.stream()
                                       .filter(l->l >350)
                                       .collect(Collectors.toList());
         logger.info("{}",integerList);
         int integer=list.stream()
                              .max((integer1,integer2)->integer1>integer2?1:-1).get();
         logger.trace("Greater number in List {}",integer);
         long count =list.stream().filter(l->l >350).count();
         logger.trace("Total Numbers in list {}",count);
         logger.info("List in Sorted order ");
         list.stream().sorted()
                      .forEach(System.out::println);
     }
 }

