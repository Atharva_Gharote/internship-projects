package com.demoprojects.EightInterface.EightClassImplementation;
import com.demoprojects.EightInterface.Netflix;
import com.demoprojects.EightInterface.Quora;

public class DefaultMethods implements Quora,Netflix
 {
    public void printMessage()
    {
        Quora.super.printMessage();
        Netflix.super.printMessage();
        Quora.displayMessage();
        Netflix.displayMessage();
    }
 }