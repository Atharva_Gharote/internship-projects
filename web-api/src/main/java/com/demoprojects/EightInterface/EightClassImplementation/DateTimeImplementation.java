package com.demoprojects.EightInterface.EightClassImplementation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.time.*;

public class DateTimeImplementation
 {
     private static final Logger logger = LogManager.getLogger(DateTimeImplementation.class);

     public static void main(String[] args)
    {
        logger.trace("Implementing LocalDateTime--");
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDate localDate =LocalDate.now();
        DayOfWeek dayOfWeek=localDate.getDayOfWeek();
        logger.info("Current Date and Time {}",localDateTime);
        logger.trace("Implementing Day of Week---");
        logger.info("Today's Date {} and Day {}",localDate,dayOfWeek);
        logger.trace("Implementing LocalDate--");
        logger.info("Pack Will renew after 30days {}",localDate.plusDays(30));
        logger.trace("Implementing ZoneDateTim--");
        ZoneId zoneId=ZoneId.of("America/New_York");
        logger.info("ZoneId  {}",zoneId);
        LocalDateTime localDateTime1=LocalDateTime.now(zoneId);
        logger.info("Zone LocalDate {}",localDateTime1);
        ZonedDateTime zonedDateTime=ZonedDateTime.of(localDateTime1,zoneId);
        logger.info("Date and Time of Zone {}",zonedDateTime);

    }
 }