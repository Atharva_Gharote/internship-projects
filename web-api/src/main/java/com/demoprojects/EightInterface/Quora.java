package com.demoprojects.EightInterface;

import com.demoprojects.EightInterface.EightClassImplementation.MainPackage.Java8Enhancements;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface Quora
    {
        static  Logger logger = LogManager.getLogger(Java8Enhancements.class);

        default void printMessage()
        {
            logger.trace("Quora is already Installed on this device");
        }
        static void displayMessage()
        {
            logger.trace("Quora is already Installed on this device");
        }
    }