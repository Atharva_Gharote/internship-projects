package com.demoprojects.EightInterface;

public interface TypeInferenceExample<K,V,R>
{
   R multiply(K k, V v);
}
