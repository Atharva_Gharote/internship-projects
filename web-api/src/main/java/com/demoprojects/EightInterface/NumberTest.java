package com.demoprojects.EightInterface;

@FunctionalInterface
   public interface NumberTest
    {
        boolean computeTest();
    }