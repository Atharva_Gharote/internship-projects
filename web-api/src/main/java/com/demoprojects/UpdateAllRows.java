package com.demoprojects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.*;

public class UpdateAllRows
 {
     private static final Logger logger = LogManager.getLogger(UpdateAllRows.class);

     public static void main(String args[])
     {
         final String sql="UPDATE users SET city=? RETURNING *";
         try(Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples",
                 "postgres", "2317");
              PreparedStatement preparedStatement = connection.prepareStatement(sql))
         {
             Class.forName("org.postgresql.Driver");
             logger.info("\nDatabase opened  successfully\n");
             preparedStatement.setString(1, "Nagpur");
             ResultSet resultset = preparedStatement.executeQuery();
             logger.info("\n" + "id firstName  lastName   email  city  courseName  user_password  creationStamp");
             while (resultset.next())
             {
                 int id = resultset.getInt("id");
                 String firstName = resultset.getString("first_name");
                 String lastName = resultset.getString("last_name");
                 String email = resultset.getString("email");
                 String city = resultset.getString("city");
                 String courseName = resultset.getString("course_name");
                 String userPassword = resultset.getString("user_password");
                 String courseDuration = resultset.getString("course_duration");
                 Timestamp creationStamp = resultset.getTimestamp("creation_stamp");
                 logger.info("\n{} {} {} {} {} {} {} {} {}", id, firstName, lastName, email, city, courseName, userPassword, courseDuration, creationStamp);

             }
             logger.trace("Information Updated");


         } catch (SQLException|ClassNotFoundException e)
         {
             logger.error(e);
         }

     }
 }
