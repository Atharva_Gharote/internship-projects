package com.demoprojects;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

 public class Log
  {

    private static final Logger logger = LogManager.getLogger(Log.class);

    private static void loggerTestingMethod()
    {
        String data = "Atharva";
        String user= new String("Watson");
        Integer x = 30;
        Double y = 30.0;

        if(data.equals(user))
        {
            logger.info("Correct Entry");
        }
        else
            {
             logger.error("Invalid String");
             logger.info("String must be {}",data);
            }

        if(x>y)
        {
            logger.info("Values {}  greater ",x);
        }
        else
            {
            logger.error("Value is less ");
            }
    }

    public static void main(String args[])
     {
       loggerTestingMethod();

        logger.log(Level.getLevel("NEW_LEVEL"), "New Level Created");
        logger.debug("Debugging is going on");
        logger.warn("You Must Insert Certain parameters");

     }
  }
