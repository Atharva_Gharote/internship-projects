package com.demoprojects;

import com.sun.istack.internal.NotNull;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.lang.*;
import java.sql.*;



public class DatabaseJDBC
 {

    private static final Logger logger = LogManager.getLogger(DatabaseJDBC.class);
    public  void insertData(Connection connection)
    {
        final String sql="INSERT INTO users (first_name,last_name,email,city,course_name,user_password,course_duration,creation_stamp)"+
                " VALUES(?,?,?,?,?,?,?,?)";

        try(PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
        passingValue(preparedStatement,"Ram","Kapoor","ramk@yahoo.com", "mumbai","JAVA","wert45",4);
        passingValue(preparedStatement,"Atharva","Gharote","atharvag@live.com","Delhi","JAVA","ath45",4);
        passingValue(preparedStatement,"mahesh","Kumar","mk233@yahoo.com",    "Lucknow","amgularjs","455556",3);
        passingValue(preparedStatement,"ramesh","jadhav","rjadhav@yahoo.com", "Shegaon",".net","btvtv5",4);
        passingValue(preparedStatement,"suresh","singh","suresh333@yahoo.com", "Mathura","c#","rvv5",4);
        passingValue(preparedStatement,"shubham","Gharote","shubhg34@yahoo.com","Budhana","java","wvv5v4",4);
        passingValue(preparedStatement,"vikas","vijaya","vv99@yahoo.com",    "Pune","java","wvvfv5",4);
        passingValue(preparedStatement,"vilas","khanna","vk776@yahoo.com","Chandrapur","python","667778",4);
        passingValue(preparedStatement,"rahul","Raj","rraj@gmai.com","kolkata","perl","222555",2);
        passingValue(preparedStatement,"Sudesh","Mishra","sushm@yahoo.com","chennai","php","w555ggg",3);
        logger.trace("Table Created");
        }
        catch (SQLException e)
        {
            logger.error(e);
        }


    }
    public  void passingValue(PreparedStatement preparedStatement,String firstName, String lastName,String email,
                              String city,String courseName,String userPassword,int courseDuration)
    {

         try
         {
             Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
             preparedStatement.setString(1, firstName);
             preparedStatement.setString(2, lastName);
             preparedStatement.setString(3, email);
             preparedStatement.setString(4, city);
             preparedStatement.setString(5, courseName);
             preparedStatement.setString(6, userPassword);
             preparedStatement.setInt(7, courseDuration);
             preparedStatement.setTimestamp(8, currentTimestamp);
             preparedStatement.executeUpdate();

         }
         catch (SQLException e)
         {
             logger.error(e);
         }

    }

    public  void showResult(Connection connection)
    {
        final String sql="SELECT * FROM users ORDER BY id";
        logger.info("\n id   firstName   lastName  email   city  courseName  userPassword  courseDuration  creationStamp");

        try(PreparedStatement  preparedStatement=connection.prepareStatement(sql);
              ResultSet resultSet= preparedStatement.executeQuery())
        {
            while (resultSet.next())
            {
                int id=resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                String city = resultSet.getString("city");
                String courseName = resultSet.getString("course_name");
                String password = resultSet.getString("user_password");
                String courseDuration = resultSet.getString("course_duration");
                Timestamp creationStamp =resultSet.getTimestamp("creation_stamp");
                logger.info("\n {}  {}  {}  {}  {}  {}  {}  {}  {}",id,firstName,lastName,email,
                             city,courseName,password,courseDuration,creationStamp);
            }

        }
        catch (SQLException e)
        {
            logger.error(e);
        }

    }
    public  void updateFirstRow(Connection connection)
    {
        final String sql="SELECT * FROM users;";

        try(PreparedStatement  preparedStatement=connection.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
             ResultSet  resultSet =  preparedStatement.executeQuery())

        {
            resultSet.absolute(1);
            resultSet.updateString("course_name","PHP");
            resultSet.updateString("city","Hyderabad");
            resultSet.updateRow();
            showResult(connection);
        }
        catch (SQLException e)
        {
            logger.error(e);
        }

    }

     public  void updateRows(Connection connection)
     {
        @NotNull final String sql="UPDATE users SET course_name=? ,city=?,course_duration=?";

        try(PreparedStatement  preparedStatement=connection.prepareStatement(sql))
        {
            preparedStatement.setString(1,"java");
            preparedStatement.setString(2,"Pune");
            preparedStatement.setInt(3,4);
            preparedStatement.addBatch();
            preparedStatement.executeBatch();
            showResult(connection);

        }
        catch (SQLException e)
        {

            logger.error(e);

        }

     }
    public  void deleteFirstRow(Connection connection)

    {
        final String sql="SELECT * FROM users";
        try(PreparedStatement  preparedStatement=connection.prepareStatement(sql,
                   ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                ResultSet resultSet =  preparedStatement.executeQuery())
        {

             resultSet.next();
             resultSet.deleteRow();
             showResult(connection);


        } catch (SQLException e)
        {
            logger.error(e);
        }


    }
     public  void deleteAllRows(Connection connection)
     {
         final String sql="DELETE FROM  users ;";

        try(PreparedStatement  preparedStatement=connection.prepareStatement(sql))
         {
             preparedStatement.executeUpdate();
         }
         catch (SQLException e)
         {
             logger.error(e);
         }


     }


   public static void main(String args[])

    {
        try(Connection  connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples",
                    "postgres", "2317"))
        {
            Class.forName("org.postgresql.Driver");
            logger.info("\nDatabase opened  successfully\n");
            DatabaseJDBC databaseJDBC=new DatabaseJDBC();
            logger.trace("\nInserting Values in Database");
            databaseJDBC.insertData(connection);
            logger.info("\n");
            logger.trace("Inserted Values in database");
            databaseJDBC. showResult(connection);
            logger.info("\n");
            logger.trace("Value of first row is updated ");
            databaseJDBC. updateFirstRow(connection);
            logger.info("\n");
            logger.trace("Data in table is updated");
            databaseJDBC. updateRows(connection);
            logger.info("\n");
            logger.trace("Deleting first row only");
            databaseJDBC. deleteFirstRow(connection);
            logger.info("\n");
            logger.trace("All Rows Deleted");
            databaseJDBC.deleteAllRows(connection);

        }
        catch (SQLException|ClassNotFoundException e)
        {
            logger.error(e);
        }


    }

 }
