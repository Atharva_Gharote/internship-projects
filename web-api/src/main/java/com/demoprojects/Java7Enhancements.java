package com.demoprojects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Java7Enhancements
 {
     private static final Logger logger = LogManager.getLogger(Java7Enhancements.class);

     public  void matchString()
    {
        String sportsName;
        Scanner scanner=new Scanner(System.in);
        logger.info("Enter sports name to determine no of players");
        sportsName=scanner.nextLine();
        switch (sportsName)
        {
            case "tennis":logger.info("number of players in each team {}",1);
                             break;
            case "volleyball":logger.info("number of players in each team {}",6);
                              break;
            case "cricket":logger.info("number of players in each team {}",11);
                             break;
            case "baseball":logger.info("number of players in eah team {}",9);
                             break;
            case "football":logger.info("number of players in each team {}",11);
                             break;
            case "basketball":logger.info("number of players in each team {}",5);
                             break;
            default :logger.info("Invalid Choice");

        }
    }
     public  void inputElementsInList(){

         List<Integer> listOfIntegers = new ArrayList<>();
         listOfIntegers.add(1788_4555);
         listOfIntegers.add(234_333);
         listOfIntegers.add(34_3944);
         listOfIntegers.add(4444_4444);

         List<Float> listOfFloats = new ArrayList<>();
         listOfFloats.add(334.444_40f);
         listOfFloats.add(144.444_0f);
         listOfFloats.add(2444.444_44f);
         listOfFloats.add(344.442f);
         displayElementsOfList(listOfIntegers,listOfFloats);

     }
    @SafeVarargs
     public static <T> void displayElementsOfList(T...lists)
    {
        logger.info("Elements of List  \n");
        for(T elements:lists)
        {
           logger.info("{}",elements);
        }


    }
    public  void tryWithResource()
    {

        final String sql="UPDATE users SET city = ? WHERE id=? ";
         try(Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples",
                    "postgres", "2317");PreparedStatement  preparedStatement =connection.prepareStatement(sql))
         {
            Class.forName("org.postgresql.Driver");
            preparedStatement.setString(1,"Nagpur");
            preparedStatement.setInt(2,6);
            preparedStatement.executeUpdate();
            DatabaseJDBC databaseJDBC=new DatabaseJDBC();
            databaseJDBC.showResult(connection);

         }
         catch(SQLException|ClassNotFoundException e)
            {
                logger.error(e);
            }

    }
    public static void main(String args[])
     {
         Java7Enhancements java7Enhancements=new Java7Enhancements();
         Scanner scanner=new Scanner(System.in);
         int binaryNumber=0b110101;
         logger.info("Decimal Value of 110101 is {}",binaryNumber);
         long number=1321_6781_67;
         logger.info("Number of Car is {}",number);
         java7Enhancements.matchString();
         List<Integer> list = new ArrayList<>();
         logger.trace("Enter elements in list\n");
         list.add(scanner.nextInt());
         list.add(scanner.nextInt());
         list.add(scanner.nextInt());
         list.add(scanner.nextInt());
         list.add(scanner.nextInt());
         logger.info("Numbers in List\n");
         list.forEach ( System.out::println);
         logger.trace("Elements updated in table users\n");
         java7Enhancements.inputElementsInList();
         logger.info("\nUpdated information in table");
         java7Enhancements.tryWithResource();

     }

 }

